﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseJoint
{
    float springConstant = 100.0f;
    float dampening = 200.0f;
    float startDistance;
    public Node Node1;
    public Node Node2;

    public BaseJoint(Node _node1, Node _node2)
    {
        Node1 = _node1;
        Node2 = _node2;
        startDistance = Vector3.Distance(Node1.transform.position, Node2.transform.position);
    }

    public virtual void Update()
    {
        var currentDistance = Vector3.Distance(Node1.transform.position, Node2.transform.position);
        var force1 = (currentDistance - startDistance) * springConstant * (Node2.transform.position - Node1.transform.position).normalized;
        force1 += ((Node2.currentPos - Node2.prevPos) - (Node1.currentPos - Node1.prevPos)) * dampening;
        var a1 = force1 / Node1.mass;
        var a2 = -a1;
        Node1.acceleration += a1;
        Node2.acceleration += a2;
    }
}
