﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Vector3 newPos;
    public Vector3 prevPos;
    public float timeStep;
    public Vector3 acceleration;
    public Vector3 gravity;
    public float mass;

    public Node Previous;
    public Node Next;
    protected List<BaseJoint> joints = new List<BaseJoint>();

    public Vector3 currentPos
    {
        get
        {
            return transform.position;
        }
        set
        {
            if (Previous != null)
                transform.position = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        // does this node have no previous? if so then it is the root node so create the joints
        if (Previous == null)
        {
            CreateJoints();
        }
        prevPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // if this is the root node then tell the joints to update
        foreach (BaseJoint joint in joints)
        {
            joint.Update();
        }
    }

    void LateUpdate()
    {
        currentPos = transform.position;
        newPos = currentPos + (currentPos - prevPos) + acceleration * timeStep * timeStep;
        prevPos = currentPos;
        currentPos = newPos;
        acceleration = gravity;
    }

    void CreateJoints()
    {
        // traverse the chain of nodes
        Node current = this;
        while (current != null)
        {
            // if there is a next node then create the joint
            if (current.Next != null)
            {
                joints.Add(new BaseJoint(current, current.Next));
            }

            current = current.Next;
        }
    }
}
