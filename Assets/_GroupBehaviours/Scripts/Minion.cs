﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion : MonoBehaviour
{
    protected Vector3 velocity = Vector3.zero;

    public float Speed = 5f;

    protected List<Minion> minions;

    // Define the priority of each flocking rule
    public float CohesionStrength = 1f;
    protected float CohesionRange = 5f;

    public float SeparationStrength = 2.5f;
    public float SeparationDistance = 1f;

    public float AlignmentStrength = 1f;
    protected float AlignmentRange = 5f;

    public virtual Vector3 Velocity
    {
        get
        {
            return velocity;
        }
        set
        {
            velocity = value;
        }
    }

    // Use this for initialization
    protected virtual void Start()
    {
        // retreive all minions
        minions = new List<Minion>(FindObjectsOfType<Minion>());
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        // Calculate and apply the flocking vector
        velocity = (velocity.normalized + Flocking_Update()).normalized * Speed;

        // Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + velocity, Color.white);

        // apply the velocity
        transform.position += velocity * Time.deltaTime;
    }

    protected Vector3 Flocking_Update()
    {
        // Calculate each of the vectors
        Vector3 cohesionVector = Flocking_CalculateCohesion(minions) * CohesionStrength;
        Vector3 separationVector = Flocking_CalculateSeparation(minions) * SeparationStrength;
        Vector3 alignmentVector = Flocking_CalculateAlignment(minions) * AlignmentStrength;

        Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + cohesionVector, Color.red);
        Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + separationVector, Color.green);
        Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + alignmentVector, Color.blue);

        // Calculate the flocking vector
        Vector3 flockingVector = cohesionVector + separationVector + alignmentVector;

        return flockingVector.normalized;
    }

    Vector3 Flocking_CalculateCohesion(List<Minion> flock)
    {
        Vector3 cohesionVec;
        Vector3 average = Vector3.zero;
        foreach (Minion minion in flock)
        {
            //if (minion != this)
            average += minion.transform.position;
        }
        average /= flock.Count;
        average.y = 0;
        cohesionVec = (average - transform.position).normalized;
        return cohesionVec;
    }

    Vector3 Flocking_CalculateSeparation(List<Minion> flock)
    {
        Vector3 separationVel = Vector3.zero;
        foreach (Minion minion in flock)
        {
            if (minion == this)
                continue;
            if (Vector3.Distance(transform.position, minion.transform.position) <= SeparationDistance)
            {
                separationVel += (transform.position - minion.transform.position);
            }
        }
        separationVel.y = 0;
        return separationVel.normalized;
    }

    Vector3 Flocking_CalculateAlignment(List<Minion> flock)
    {
        Vector3 average = Vector3.zero;
        foreach (Minion minion in flock)
        {
            //if (minion != this)
            average += minion.velocity;
        }
        average /= flock.Count;
        average.y = 0;
        return average.normalized;
    }
}
